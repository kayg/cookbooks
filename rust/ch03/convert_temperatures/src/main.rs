use std::io;
include!("../../../helpers/strtonum.rs");

fn main() {
    let choice = strtonum("Temperature in fahrenheit or celsius? Enter 1 or 2: ");

    match choice as i32 {
        1 => {
            let fahr = strtonum("Please enter temperature in fahrenheit: ");
            print!("Temperature in celsius: {}\n", fahr_to_cel(fahr));
        },
        2 => {
            let cel = strtonum("Please enter temperature in celsius: ");
            print!("Temperature in fahrenheit: {}\n", cel_to_fahr(cel));
        },
        _ => {
            print!("Ho! Ho! Ho! Not so fast, smarty pants!");
        },
    };
}

fn fahr_to_cel(fahr: f64) -> f64 {
    (((5.0 / 9.0) * fahr) - 32.0)
}

fn cel_to_fahr(cel: f64) -> f64 {
    (((9.0 / 5.0) * cel) + 32.0)
}
