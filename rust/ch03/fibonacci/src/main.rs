use std::io;
include!("../../../helpers/strtonum.rs");

fn main() {
    let num = strtonum("Enter a number: ");

    let fibo_loop = fibo_loop(num as i32);
    println!("{}th fibonacci number (using loops): {}", num, fibo_loop);

    let fibo_recur = fibo_recur(num as i32);
    println!("{}th fibonacci number (using recursion): {}", num, fibo_recur);
}

fn fibo_loop(num: i32) -> i32 {
    let mut fibo_first = 0;
    let mut fibo_second = 1;
    let mut fibo = 0;

    for _iter in 2..num {
        fibo = fibo_first + fibo_second;

        fibo_first = fibo_second;
        fibo_second = fibo;
    }

    return fibo;
}

fn fibo_recur(num: i32) -> i32 {
    if num == 2 {
        return 1;
    } else if num == 1 {
        return 0;
    } else {
        return fibo_recur(num - 1) + fibo_recur(num - 2);
    }
}
