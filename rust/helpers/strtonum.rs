use std::io::Write;

fn strtonum(message: &str) -> f64 {
    loop {
        print!("{}", message);
        io::stdout().flush().unwrap();

        let mut _generic_string = String::new();

        io::stdin()
            .read_line(&mut _generic_string)
            .expect("Failed to read line.");

        let _generic_string = match _generic_string
            .trim()
            .parse() {
                Ok(num) => {
                    return num;
                },
                Err(_) => {
                    continue;
                },
            };
    };
}
